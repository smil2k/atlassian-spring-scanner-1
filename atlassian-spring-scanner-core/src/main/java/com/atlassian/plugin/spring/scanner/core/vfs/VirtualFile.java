package com.atlassian.plugin.spring.scanner.core.vfs;

import java.io.IOException;
import java.util.Collection;
import java.util.Properties;

/**
 * A virtual file interface to abstract the differences between Javac Filer disk access and bog standard File access.
 */
@SuppressWarnings ("UnusedDeclaration")
public interface VirtualFile
{

    Collection<String> readLines() throws IOException;

    void writeLines(Iterable<String> lines) throws IOException;

    void writeProperties(Properties properties, String comment) throws IOException;
}
