package com.atlassian.plugin.spring.scanner.dynamic.contexts;

import org.eclipse.gemini.blueprint.context.support.OsgiBundleXmlApplicationContext;
import org.springframework.beans.factory.xml.XmlBeanDefinitionReader;
import org.springframework.context.ApplicationContext;

/**
 * This is a copy of the NonValidatingOsgiBundleXmlApplicationContext that is in atlassian-osgi-spring-extender.  Speaking
 * to Don Brown about it he said the non validating version is faster for plugin startup which is why it was created.
 *
 * It had to be copied since it hid the constructor that allows you to specify a parent context which is what we
 * need if we are to have child-parent context relationships.  This can be fixed upstream but for now it really such
 * little code.
 *
 * Original comments follow :
 *
 * Application context that initializes the bean definition reader to not validate via XML Schema.  Note that by
 * turning this off, certain defaults won't be populated like expected.  For example, XML Schema provides the default
 * autowire value of "default", but without this validation, that value is not set so autowiring will be turned off.
 * <p/>
 * This class exists in the same package as the parent so the log messages won't get confused as the parent class
 * logs against the instance class.
 *
 * @since 2.5.0
 */
public class GeminiOsgiBundleXmlApplicationContext extends OsgiBundleXmlApplicationContext implements OsgiBundleApplicationContext
{
    public GeminiOsgiBundleXmlApplicationContext(String[] configLocations, ApplicationContext parent)
    {
        super(configLocations, parent);
    }

    @Override
    protected void initBeanDefinitionReader(XmlBeanDefinitionReader beanDefinitionReader)
    {
        super.initBeanDefinitionReader(beanDefinitionReader);
        beanDefinitionReader.setValidationMode(XmlBeanDefinitionReader.VALIDATION_NONE);
        beanDefinitionReader.setNamespaceAware(true);
    }
}
