package com.atlassian.plugin.spring.scanner.test;

import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsService;

import org.springframework.beans.factory.DisposableBean;
import org.springframework.stereotype.Component;

@ExportAsService({ExposedAsAServiceComponentInterface.class, ExposedAsASecondComponentInterface.class})
@Component
public class ExposedAsAServiceComponentWithMultipleSpecifiedInterfaces implements
        ExposedAsAServiceComponentInterface, ExposedAsASecondComponentInterface, DisposableBean
{
    @Override
    public void destroy() throws Exception
    {
    }

    @Override
    public void doStuff()
    {
    }

    @Override
    public void doOtherStuff()
    {
    }
}
