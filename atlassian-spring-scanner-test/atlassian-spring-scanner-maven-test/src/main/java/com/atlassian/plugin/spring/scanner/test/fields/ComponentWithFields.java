package com.atlassian.plugin.spring.scanner.test.fields;

import com.atlassian.jira.bc.issue.link.IssueLinkService;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.plugin.spring.scanner.test.NamedComponent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ComponentWithFields
{
    @ComponentImport
    IssueLinkService issueLinkService;

    @Autowired
    NamedComponent namedComponent;
}
