package com.atlassian.plugin.spring.scanner.processor;

import java.lang.annotation.Annotation;
import java.util.Set;

import javax.annotation.processing.RoundEnvironment;
import javax.annotation.processing.SupportedAnnotationTypes;
import javax.lang.model.element.*;
import javax.tools.Diagnostic;

import com.atlassian.plugin.spring.scanner.util.CommonConstants;

/**
 * Handles Springs @Component and our product specific *Component annotations and creates class index files for them.
 * Cross product components are listed in META-INF/plugin-components/components
 * Product specific components are listed in META-INF/plugin-components/components-${productName}
 * 
 * Entries in these files are either just the fully qualified class name of the component, or the fully qualified class name
 * plus the bean name defined as the value of the annotation separated by #
 * 
 * Example:
 * 
 * com.some.component.without.a.name.MyClass
 * com.some.component.with.a.name.MyClass#myBeanName
 */
@SuppressWarnings ("UnusedDeclaration")
@SupportedAnnotationTypes({
        "org.springframework.stereotype.Component",
        "org.springframework.stereotype.Service",
        "org.springframework.stereotype.Controller",
        "org.springframework.stereotype.Repository",
        "javax.inject.Named",
        "com.atlassian.plugin.spring.scanner.annotation.component.*"
})
public class ComponentAnnotationProcessor extends IndexWritingAnnotationProcessor
{
    @Override
    public boolean process(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv)
    {
        doProcess(annotations,roundEnv, CommonConstants.COMPONENT_KEY);
        
        return false;
    }

    @Override
    public TypesAndAnnotation getTypeAndAnnotation(Element element, TypeElement annotationTypeElement)
    {
        TypeElement typeElement = null;
        Annotation annotation;
        TypesAndAnnotation typesAndAnnotation = null;
        Class componentAnnotationClass;
        try
        {
            componentAnnotationClass = Class.forName(annotationTypeElement.getQualifiedName().toString());
        }
        catch (ClassNotFoundException e)
        {
            return null;
        }

        if (element instanceof TypeElement)
        {
            typeElement = (TypeElement) element;
            annotation = typeElement.getAnnotation(componentAnnotationClass);

            typesAndAnnotation = new TypesAndAnnotation(typeElement, typeElement, annotation);
        }
        else if (element instanceof VariableElement)
        {
            VariableElement variableElement = (VariableElement) element;
            if (isParameterElement(variableElement) && ElementKind.CONSTRUCTOR.equals(variableElement.getEnclosingElement().getKind()))
            {
                typeElement = (TypeElement) processingEnv.getTypeUtils().asElement(variableElement.asType());
                annotation = variableElement.getAnnotation(componentAnnotationClass);
                typesAndAnnotation = new TypesAndAnnotation(typeElement, getContainingClass(variableElement), annotation);
            }
            if (isFieldElement(variableElement) && ElementKind.CLASS.equals(variableElement.getEnclosingElement().getKind()))
            {
                typeElement = (TypeElement) processingEnv.getTypeUtils().asElement(variableElement.asType());
                annotation = variableElement.getAnnotation(componentAnnotationClass);
                typesAndAnnotation = new TypesAndAnnotation(typeElement, getContainingClass(variableElement), annotation);
            }
        }

        if(null != typeElement && !ElementKind.CLASS.equals(typeElement.getKind()))
        {
            String message = "Annotation processor found a type [" + typeElement.getQualifiedName().toString() + "] annotated as a component, but the type is not a concrete class. NOT adding to index file!!";
            processingEnv.getMessager().printMessage(Diagnostic.Kind.MANDATORY_WARNING,message);

            typesAndAnnotation = null;
        }

        if(null != typeElement && typeElement.getModifiers().contains(Modifier.ABSTRACT))
        {
            String message = "Annotation processor found a type [" + typeElement.getQualifiedName().toString() + "] annotated as a component, but the type is abstract. NOT adding to index file!!";
            processingEnv.getMessager().printMessage(Diagnostic.Kind.MANDATORY_WARNING,message);

            typesAndAnnotation = null;
        }
        
        return typesAndAnnotation;
    }

    private boolean isFieldElement(final VariableElement variableElement)
    {
        return ElementKind.FIELD.equals(variableElement.getKind());
    }

    private boolean isParameterElement(final VariableElement variableElement)
    {
        return ElementKind.PARAMETER.equals(variableElement.getKind());
    }
}
