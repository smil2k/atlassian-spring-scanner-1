package com.atlassian.plugin.spring.scanner.extension.testservices;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class ProxyEnforcerAspect {

    @Around("execution(public * com.atlassian.plugin.spring.scanner.extension.testservices.*Proxy.*(..))")
    public Object around(ProceedingJoinPoint method) throws Throwable {
        return method.proceed();
    }
}
