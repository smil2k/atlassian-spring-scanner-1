package com.atlassian.plugin.spring.scanner.extension;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;
import org.springframework.beans.BeansException;
import org.springframework.beans.PropertyValues;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.config.DestructionAwareBeanPostProcessor;
import org.springframework.beans.factory.config.InstantiationAwareBeanPostProcessor;

import java.beans.PropertyDescriptor;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Dictionary;
import java.util.Enumeration;

import static java.lang.String.format;

/**
 * A BeanPostProcessor that prints information about the bean before/after initialisation so developers
 * can debug their Spring context
 */
public class DevModeBeanInitialisationLoggerBeanPostProcessor
        implements InstantiationAwareBeanPostProcessor, InitializingBean, DestructionAwareBeanPostProcessor, DisposableBean
{
    private static final boolean isDevMode = Boolean.parseBoolean(System.getProperty("atlassian.dev.mode", "false"));

    /**
     * We have one logger per plugin so it possible to debug any one plugin at a time
     */
    private volatile Log log;

    private final BundleContext bundleContext;

    public DevModeBeanInitialisationLoggerBeanPostProcessor(final BundleContext bundleContext)
    {
        this.bundleContext = bundleContext;
    }


    @Override
    public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException
    {
        return bean;
    }

    @Override
    public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException
    {
        logBeanDetail("AfterInitialisation", bean.getClass(), beanName);
        return bean;
    }

    @Override
    public Object postProcessBeforeInstantiation(Class beanClass, String beanName) throws BeansException
    {
        logBeanDetail("BeforeInstantiation", beanClass, beanName);
        return null; // do default instantiation
    }

    @Override
    public boolean postProcessAfterInstantiation(Object bean, String beanName) throws BeansException
    {
        return true; // don't skip property setting
    }

    @Override
    public PropertyValues postProcessPropertyValues(PropertyValues pvs, PropertyDescriptor[] pds, Object bean, String beanName) throws BeansException
    {
        return pvs; // pass values to be set through
    }

    @Override
    public void postProcessBeforeDestruction(final Object bean, final String beanName) throws BeansException
    {
        logBeanDetail("BeforeDestruction", bean.getClass(), beanName);
    }

    private void logBeanDetail(String stage, Class beanClass, String beanName)
    {
        if (log.isDebugEnabled())
        {
            log.debug(format("%s [bean=%s, type=%s]", stage, beanName, beanClass.getName()));
        }
    }

    @Override
    public void afterPropertiesSet() throws Exception
    {
        // only log the startup info once, the first time we are hit
        log = LogFactory.getLog(getLoggerName());
        Bundle bundle = bundle();
        if (isDevMode)
        {
            String loggerName = getLoggerName();
            // this has to be WARN since containers like JIRA have WARN as their default root levels
            String msg = format(""
                            + "\n"
                            + "Spring context started for bundle : %s id(%d) v(%s) %s"
                            + "\n\n"
                            + "If you want to debug the Spring wiring of your code then set a DEBUG level log level as follows.  [ This is a dev.mode only message. ]"
                            + "\n"
                            + "\tlog4j.logger.%s  = DEBUG, console, filelog"
                            + "\n"
                            + "",

                    bundle.getSymbolicName(),
                    bundle.getBundleId(),
                    bundle.getVersion(),
                    bundle.getLocation(),
                    loggerName
                    );
            log.warn(msg);
        }
        printBundleDebugInfo(bundle);
    }

    @Override
    public void destroy() throws Exception
    {
        if (isDevMode)
        {
            Bundle bundle = bundle();
            log.warn(String.format("\n\n\tSpring context destroyed : %s id(%d) v(%s) \n", bundle.getSymbolicName(), bundle.getBundleId(), bundle.getVersion()));
        }
    }

    private void printBundleDebugInfo(final Bundle bundle)
    {
        if (log.isDebugEnabled())
        {
            StringWriter sw = new StringWriter();
            PrintWriter out = new PrintWriter(sw);

            out.println();
            out.format("\tBundle Id : %d\n", bundle.getBundleId());
            out.format("\tBundle Name : %s\n", bundle.getSymbolicName());
            out.format("\tBundle Location : %s\n", bundle.getLocation());
            out.format("\tBundle Version : %s\n", bundle.getVersion());

            out.format("\tBundle Headers :\n");
            Dictionary headers = bundle.getHeaders();
            Enumeration keys = headers.keys();
            while (keys.hasMoreElements())
            {
                Object key = keys.nextElement();
                Object value = headers.get(key);
                out.format("\t\t%s: %s\n", key, value);
            }
            out.println();

            log.debug(sw.toString());
        }
    }

    private Bundle bundle()
    {
        return bundleContext.getBundle();
    }

    private String getLoggerName()
    {
        return String.format("%s.spring", bundle().getSymbolicName());
    }
}
