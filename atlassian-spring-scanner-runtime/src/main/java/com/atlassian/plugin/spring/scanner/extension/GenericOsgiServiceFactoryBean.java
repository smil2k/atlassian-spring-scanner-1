package com.atlassian.plugin.spring.scanner.extension;

import org.osgi.framework.ServiceRegistration;

/**
 * An implementation agnostic OsgiServiceFactoryBean interface that doesn't care
 * if it's implemented by SpringDM or Gemini Blueprint.
 */
public interface GenericOsgiServiceFactoryBean
{
    ServiceRegistration getObject() throws Exception;

    void destroy();
}
