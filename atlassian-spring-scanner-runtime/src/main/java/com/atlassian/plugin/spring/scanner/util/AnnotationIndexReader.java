package com.atlassian.plugin.spring.scanner.util;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import com.atlassian.plugin.spring.scanner.ProductFilter;

import com.google.common.base.Charsets;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;

import static org.apache.commons.lang.StringUtils.defaultIfBlank;
import static org.apache.commons.lang.StringUtils.isNotBlank;

/**
 * A utility class to read index files from a classloader or bundle.
 * Can handle reading a single index file, or concating multiple index files based on the
 * currently running product.
 * 
 */
public class AnnotationIndexReader
{
    private static final Log log = LogFactory.getLog(AnnotationIndexReader.class);

    /**
     * Read a file from a bundle and return the list of lines of the file.
     *
     * @param resourceFile the path to the file in the bundle
     * @param bundle the bundle to read from
     * @return the lines in the file
     */
    public static List<String> readIndexFile(final String resourceFile, final Bundle bundle)
    {
        final URL url = bundle.getResource(resourceFile);
        return readIndexFile(url);
    }

    /**
     * Read a file from a classloader and return the list of lines of the file.
     *
     * @param resourceFile the path to the file in the bundle
     * @param classLoader the ClassLoader to read from
     * @return the lines in the file
     */
    public static List<String> readIndexFile(final String resourceFile, final ClassLoader classLoader)
    {
        final URL url = classLoader.getResource(resourceFile);
        final List<String> strings = readIndexFile(url);

        if (log.isDebugEnabled())
        {
            log.debug("Read annotation index file: " + resourceFile);
            log.debug("Printing out found annotated beans: ");
            log.debug(strings);
        }

        return strings;
    }

    /**
     * Read both the cross-product file and the product-specific file for the currently running product from the bundle and returns
     * their lines as a single list.
     * 
     * @param resourceFile the path to the cross-product file in the bundle
     * @param bundle the bundle to read from
     * @return the lines in the file
     */
    public static List<String> readAllIndexFilesForProduct(final String resourceFile, final Bundle bundle)
    {
        final List<String> entries = new ArrayList<String>();
        
        final URL url = bundle.getResource(resourceFile);
        
        entries.addAll(readIndexFile(url));

        final ProductFilter filter = ProductFilterUtil.getFilterForCurrentProduct(bundle.getBundleContext());
        if (null != filter)
        {
            entries.addAll(readIndexFile(filter.getPerProductFile(resourceFile), bundle));
        }

        return entries;
    }

    /**
     * Read both the cross-product file and the product-specific file for the currently running product from the classloader and
     * returns their lines as a single list.
     *
     * @param resourceFile the path to the cross-product file in the bundle
     * @param classLoader the ClassLoader to read from
     * @param bundleContext the bundle context used to determine the running product
     * @return the lines in the file
     */
    public static List<String> readAllIndexFilesForProduct(
            final String resourceFile, final ClassLoader classLoader, final BundleContext bundleContext)
    {
        final List<String> entries = new ArrayList<String>();

        final URL url = classLoader.getResource(resourceFile);

        entries.addAll(readIndexFile(url));

        final ProductFilter filter = ProductFilterUtil.getFilterForCurrentProduct(bundleContext);
        if (null != filter)
        {
            entries.addAll(readIndexFile(filter.getPerProductFile(resourceFile), classLoader));
        }

        return entries;
    }

    public static List<String> readIndexFile(final URL url)
    {
        final List<String> resources = new ArrayList<String>();

        try
        {
            if (null == url)
            {
                return resources;
            }

            final BufferedReader reader;
            try
            {
                reader = new BufferedReader(new InputStreamReader(url.openStream(), Charsets.UTF_8));
            }
            catch (final FileNotFoundException e)
            {
                return resources;
            }

            String line = reader.readLine();
            while (line != null)
            {
                resources.add(line);

                line = reader.readLine();
            }

            reader.close();
        }
        catch (final IOException e)
        {
            throw new RuntimeException("Cannot read index file [" + url.toString() + "]", e);
        }

        return resources;
    }

    public static Properties readPropertiesFile(final URL url)
    {
        final Properties resources = new Properties();
        try
        {
            if (null == url)
            {
                return resources;
            }

            final BufferedReader reader;
            try
            {
                reader = new BufferedReader(new InputStreamReader(url.openStream(), Charsets.UTF_8));
            }
            catch (final FileNotFoundException e)
            {
                return resources;
            }

            resources.load(reader);
        }
        catch (final IOException e)
        {
            throw new RuntimeException("Cannot read properties file [" + url.toString() + "]", e);
        }
        return resources;
    }

    public static String[] splitProfiles(final String profiles)
    {
        return isNotBlank(profiles) ? StringUtils.split(profiles, ',') : new String[0];
    }

    public static Iterable<String> getIndexFilesForProfiles(final String[] profileNames, final String indexFileName)
    {
        final List<String> filesToRead = new ArrayList<String>();
        if (profileNames.length > 0)
        {
            for (String profileName : profileNames)
            {
                profileName = defaultIfBlank(profileName, "").trim();
                if (isNotBlank(profileName))
                {
                    final String fileToRead = CommonConstants.INDEX_FILES_DIR + "/"
                            + CommonConstants.PROFILE_PREFIX + profileName + "/"
                            + indexFileName;

                    filesToRead.add(fileToRead);
                }
            }
        }
        else
        {
            final String fileToRead = CommonConstants.INDEX_FILES_DIR + "/" + indexFileName;
            filesToRead.add(fileToRead);
        }
        return filesToRead;
    }
}

