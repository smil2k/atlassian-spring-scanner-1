package com.atlassian.plugin.spring.scanner.extension.springdm;

import com.atlassian.plugin.spring.scanner.extension.GenericOsgiServiceFactoryBean;
import com.atlassian.plugin.spring.scanner.extension.OsgiServiceFactoryBeanFactory;
import org.osgi.framework.BundleContext;
import org.springframework.osgi.service.exporter.support.AutoExport;
import org.springframework.osgi.service.exporter.support.ExportContextClassLoader;
import org.springframework.osgi.service.exporter.support.OsgiServiceFactoryBean;
import org.springframework.osgi.service.importer.support.OsgiServiceProxyFactoryBean;

import java.util.Map;


public class SpringOsgiServiceFactoryBeanFactory implements OsgiServiceFactoryBeanFactory
{
    @Override
    public GenericOsgiServiceFactoryBean createExporter(final BundleContext bundleContext, final Object bean, final String beanName, final Map<String, Object> serviceProps, final Class<?>[] interfaces)
            throws Exception
    {
        serviceProps.put("org.springframework.osgi.bean.name", beanName);

        final OsgiServiceFactoryBean exporter = new OsgiServiceFactoryBean();
        exporter.setAutoExport(AutoExport.DISABLED);
        exporter.setBeanClassLoader(bean.getClass().getClassLoader());
        exporter.setBeanName(beanName);
        exporter.setBundleContext(bundleContext);
        exporter.setContextClassLoader(ExportContextClassLoader.UNMANAGED);
        exporter.setInterfaces(interfaces);
        exporter.setServiceProperties(serviceProps);
        exporter.setTarget(bean);

        exporter.afterPropertiesSet();

        return new SpringOsgiServiceFactoryBean(exporter);
    }

    @Override
    public Class getProxyClass()
    {
        // This class is explicitly mentioned here (Rather than using Class.forName() for example in SpringDMUtil) to make sure
        // BND will generate an optional package import for this class!
        return OsgiServiceProxyFactoryBean.class;
    }
}
