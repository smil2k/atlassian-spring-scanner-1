package com.atlassian.plugin.spring.scanner.annotation.component;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * This annotation makes the class as being interesting in terms of scanning but it is not a component that needs to be
 * imported or instantiated.  Its a candidate for scanning say as a placeholder for @ ComponentImport or
 * @ ClasspathComponent type fields.
 */
@Target ({ ElementType.TYPE })
@Retention (RetentionPolicy.RUNTIME)
public @interface Scanned
{
}